#!/bin/bash

echo "enter user name"
read user
echo "pi IP"
read piIP

echo "#$user keys" >> keys
ssh root@nova.chat '/root/wgscript.sh' >> keys
#sudo docker run -it cmulk/wireguard-docker /bin/bash -c 'wg genkey | tee privatekey | wg pubkey > publickey && echo pub=`cat publickey` && echo pri=`cat privatekey`' >> keys
sed -i 's/\r//g' keys
source keys

# configure wireguard server
ssh root@nova.chat 'cp /etc/wireguard/wg0.conf /etc/wireguard/wg0.conf.bak'
scp root@nova.chat:/etc/wireguard/wg0.conf temp

octet=$(($(grep /32 temp | tail -1 | sed -E 's/[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)\/[0-9]+/\1/') + 1))
echo "octet=$octet" > $user-configs
ip="10.0.0.$octet"
oldip="AllowedIPs = 10.0.0.$(($octet-1))\/32"
cleanpub=`echo $pub | sed 's:/:\\\/:g'`
sed -i '/'"$oldip"'/a\ \n[Peer]\nPublicKey = '"$cleanpub"'\nAllowedIPs = '"$ip"'\/32' temp

scp temp root@nova.chat:/etc/wireguard/wg0.conf
ssh root@nova.chat 'systemctl restart wg-quick@wg0'
ssh root@nova.chat 'systemctl status wg-quick@wg0 | grep Active'
echo "#$user's IP: $ip" >> keys
rm temp

# set up client wg config
cleanpri=`echo $pri | sed 's:/:\\\/:g'`
echo "cleanpri=$cleanpri" >> $user-configs
cp wireguard/wg0-clienttemplate.conf wg0-$user.conf
sed -i 's/$(privatekey)/'"$cleanpri"'/g;s/$(ip)/'"$ip"'/g' wg0-$user.conf

scp wg0-$user.conf ubuntu@$piIP:/home/ubuntu/wg0.conf
ssh ubuntu@$piIP 'sudo cp /home/ubuntu/wg0.conf /etc/wireguard/wg0.conf && sudo systemctl restart wg-quick@wg0'
ssh root@nova.chat 'wg show'

# set up haproxy
ssh root@nova.chat 'cd /srv/docker/haproxy && cp haproxy.cfg haproxy.cfg.bak'
scp root@nova.chat:/srv/docker/haproxy/haproxy.cfg .
sed -i '/addhttpsfrontend/a \ \ use_backend '"$user"' if \{ req\.ssl_sni \-i '"$user"'\.nova\.chat \}' haproxy.cfg
sed -i '/addhttpfrontend/a \ \ acl '"$user"'_request hdr\(host\) \-i '"$user"'\.nova\.chat\n\ \ use_backend '"$user"'_pub if '"$user"'_request\n' haproxy.cfg
sed -i '/addhttpsbackend/a backend '"$user"'\n\ \ mode tcp\n\ \ balance roundrobin\n\ \ option ssl\-hello\-chk\n\ \ server pi 10\.0\.0\.'"$octet"'\:443 check sni req\.ssl\_sni send\-proxy\n' haproxy.cfg
sed -i '/addhttpbackend/a backend '"$user"'_pub\n\ \ server pi 10\.0\.0\.'"$octet"':80 check\n ' haproxy.cfg
scp haproxy.cfg root@nova.chat:/srv/docker/haproxy/haproxy.cfg
ssh root@nova.chat 'service haproxy restart'
rm haproxy.cfg
#second time for good luck
ssh root@nova.chat 'service haproxy restart'
rm haproxy.cfg 
echo "haproxy set up"

# set up pi
ssh ubuntu@$piIP 'sudo apt install -y docker.io pwgen git python3-setuptools python3-pip && sudo pip3 install docker-compose'
ssh ubuntu@$piIP 'sudo usermod -aG docker ubuntu'
ssh ubuntu@$piIP 'git clone https://gitlab.com/nova/pi-matrix.git && sudo systemctl stop synapse && sudo systemctl disable synapse'
echo "user="$user >> $user-configs
#echo "sharedSecret=`pwgen -s 128 1`" >> $user-configs
#echo "DB_PASSWORD=`pwgen -s 64 1`" >> $user-configs
scp $user-configs ubuntu@$piIP:/home/ubuntu/pi-matrix/configs
echo "sent configs to pi and finished"
#ssh ubuntu@$piIP 'cd pi-matrix && ./configure.sh'






