# pi-config

Steps to install
1. grab ubuntu 64 bit image from https://sfo2.digitaloceanspaces.com/novasw/ubuntu-2020-01-17.img.xz
2. un7z then etch to 16gb+ SD
3. then resize to fit SD
```
# resize rootfs to fill card
sudo parted -s /dev/sdX resizepart 2 '100%'
sudo resize2fs /dev/sdX2
```
4. get eric to run server.sh and send you 
```
udo apt install -y docker.io && sudo apt-get install -y python3-pip && sudo pip3 install docker-compose
sudo usermod -aG docker ubuntu
#then logout (exit) and ssh back in
```
5. `cd ~/`
6. `git clone https://gitlab.com/nova/pi-matrix.git`
7. `cd pi-matrix`
8. `./configure.sh`
